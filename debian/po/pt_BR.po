# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Translation of Debian Installer templates to Brazilian Portuguese.
# This file is distributed under the same license as debian-installer.
#
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2008-2012.
# Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2010-2015.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001-2002.
#   Free Software Foundation, Inc., 2000
#   Juan Carlos Castro y Castro <jcastro@vialink.com.br>, 2000-2005.
#   Leonardo Ferreira Fontenelle <leonardof@gnome.org>, 2006-2009.
#   Lisiane Sztoltz <lisiane@conectiva.com.br>
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: rescue@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:37+0100\n"
"PO-Revision-Date: 2015-05-15 15:49-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@arg.eti.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Modo de recuperação (\"rescue\")"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Entrar em modo de recuperação"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Nenhuma partição encontrada"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"O instalador não pode encontrar nenhuma partição, portanto você não será "
"capaz de montar o sistema de arquivos raiz. Isto pode ser causado por um "
"kernel que falhou ao detectar a sua unidade de disco rígido ou que falhou ao "
"ler a tabela de partições, ou o disco pode não estar particionado. Se você "
"quiser, você pode investigar o problema a partir de um shell no ambiente do "
"instalador."

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Assemble RAID array"
msgstr "Montar agrupamento RAID"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Do not use a root file system"
msgstr "Não usar um sistema de arquivos raiz"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid "Device to use as root file system:"
msgstr "Dispositivo a ser usado como sistema de arquivos raiz:"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Informe um dispositivo que você gostaria de usar como seu sistema de "
"arquivos raiz. Você poderá escolher entre diversas operações de recuperação "
"a serem executadas nesse sistema de arquivos."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"If you choose not to use a root file system, you will be given a reduced "
"choice of operations that can be performed without one. This may be useful "
"if you need to correct a partitioning problem."
msgstr ""
"Se você escolher não usar um sistema de arquivos raiz, você terá um conjunto "
"reduzido de operações que podem ser realizadas. Isto pode ser útil se você "
"precisar corrigir um problema de particionamento."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Dispositivo não encontrado"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"O dispositivo que você informou para seu sistema de arquivos raiz "
"(${DEVICE}) não existe. Por favor, tente novamente."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Montagem falhou"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Um erro ocorreu durante a montagem do dispositivo que você informou como seu "
"sistema de arquivos raiz (${DEVICE}) em /target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Por favor, confira o syslog para mais informações."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Operações de recuperação"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Operação de recuperação falhou"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"A operação de recuperação '${OPERATION}' falhou com o código de saída "
"${CODE}."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Executar um shell em ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Executar um shell no ambiente do instalador"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Escolher um sistema de arquivos raiz diferente"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Reinicializar o sistema"

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Executando um shell"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Após esta mensagem, você receberá um shell com ${DEVICE} montado em \"/\". "
"Se você precisar de quaisquer outros sistemas de arquivos (como um \"/usr\" "
"separado), você terá que montá-los manualmente."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Erro executando shell em /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Um shell (${SHELL}) foi encontrado em seu sistema de arquivos raiz "
"(${DEVICE}), mas um erro ocorreu no momento de sua execução."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Nenhum shell encontrado em /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Não foi encontrado nenhum shell utilizável em seu sistema de arquivos raiz "
"(${DEVICE})."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Shell interativo em ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Após esta mensagem, você receberá um shell com ${DEVICE} montado em \"/target"
"\". Você pode trabalhar no mesmo usando as ferramentas disponíveis no "
"ambiente do instalador. Se você quiser torná-lo seu sistema de arquivos raiz "
"temporariamente, execute o comando \"chroot /target\". Se você precisar de "
"quaisquer outros sistemas de arquivos (como um \"/usr\" separado), você terá "
"que montá-los manualmente."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"No file systems have been mounted."
msgstr ""
"Após esta mensagem, você receberá um shell no ambiente do instalador. Nenhum "
"sistema de arquivos foi montado."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Shell interativo no ambiente do instalador"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Frase secreta para ${DEVICE}:"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr ""
"Por favor, informe a frase secreta para o volume criptografado ${DEVICE}."

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Se você não informar algo, o volume não estará disponível durante as "
"operações de recuperação."

#. Type: multiselect
#. Choices
#. :sl3:
#: ../rescue-mode.templates:20001
msgid "Automatic"
msgstr "Automático"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid "Partitions to assemble:"
msgstr "Partições para montar:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Select the partitions to assemble into a RAID array. If you select "
"\"Automatic\", then all devices containing RAID physical volumes will be "
"scanned and assembled."
msgstr ""
"Selecione as partições para montar em um agrupamento RAID. Se você "
"selecionar \"Automático\", todos os dispositivos contendo volumes físicos "
"RAID serão verificados e montados."

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Note that a RAID partition at the end of a disk may sometimes cause that "
"disk to be mistakenly detected as containing a RAID physical volume. In that "
"case, you should select the appropriate partitions individually."
msgstr ""
"Note que uma partição RAID no final de um disco pode, algumas vezes, fazer "
"com que esse disco seja detectado erroneamente como contendo um volume "
"físico RAID. Nesse caso, você deverá selecionar individualmente as partições "
"apropriadas."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "Mount separate ${FILESYSTEM} partition?"
msgstr "Montar partição ${FILESYSTEM} separada?"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "The installed system appears to use a separate ${FILESYSTEM} partition."
msgstr "O sistema instalado parece usar uma partição ${FILESYSTEM} separada."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid ""
"It is normally a good idea to mount it as it will allow operations such as "
"reinstalling the boot loader. However, if the file system on ${FILESYSTEM} "
"is corrupt then you may want to avoid mounting it."
msgstr ""
"É geralmente uma boa ideia montar a partição, pois isso permitirá operações "
"tais como a reinstalação do carregador de inicialização. Contudo, se o "
"sistema de arquivos em ${FILESYSTEM} estiver corrompido, então você pode "
"querer evitar montá-lo."
